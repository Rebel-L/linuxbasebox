Linux Base Box Project
=================================================

0.	Index of Contents
=================================================

1.	Description
2.	Requirements
	1.	Basic Requirements
	2.	Additional Requirements for Vagrant
3.	List of Basic Applications
4.	HowTo Use It



1.	Description
=================================================

This project is mainly used for a basic linux system setup. Every scripts can be used to create basic server images or
to create base boxes for vagrant / virtual box.
For use with vagrant there are some additional requirements. Please check chapter 2.2 for more details.



2.	Requirements
=================================================
In the following the requirements for running the scripts are listed.


2.1 Basic Requirements
-------------------------------------------------

There are no basic requirements anymore, since bitbuckt support downloading repository as gzip files.


2.2 Additional Requirements for Vagrant
-------------------------------------------------

For building a base box you should set up the virtual box with minimum ressources. I recommend the following:
- 512MB RAM
- 8GB virtual disc (dynamic sized)
- 64 Bit
- Set System > Motherboard > Enable IO APIC to true
- Set System > Processor > Enable PAE/NX to true
- Disable audio
- Disable floppy drive

During your installation of your linux distribution, you have to setup a specific user, so that vagrant is able to do
its work. This user must have the following requirements:

- Fullname: vagrant
- Username: vagrant
- Password: vagrant



3.	List of Basic Applications
=================================================

The following is a list of application which will be installed by the scripts.

- System Update
- SSH (can be deactivated, see chapter 4.)
- VIM
- HTOP
- Linux Headers
- SUDO (e.g. Debian)
- Ruby
- Chef (you can choose Chef only, see chapter 4.)
- Puppet (you can choose Puppet only, see chapter 4.)

The following applications are installed for vagrant base boxes only:
- Virtual Box Guest Additions
- Vagrant Keys



4.	HowTo Use It
=================================================
- Download and unextract LinuxBaseBox Repository
~~~~
wget https://bitbucket.org/Rebel-L/linuxbasebox/get/[version].tar.gz
tar xfvz [version].tar.gz
~~~~
[version] e.g. is 0.1.1 ... for actual version have a look on https://bitbucket.org/Rebel-L/linuxbasebox/downloads#tag-downloads

- Prepare script for execution (e.g. Ubuntu)
~~~~
cd linuxbasebox/Ubuntu
chmod +x post-install.sh
~~~~
- Start script (e.g. for Ubuntu base server)
~~~~
sudo -i
./post-install.sh
~~~~
- Start script for VagrantBox with VirtualBox 4.1.12 (e.g. Ubuntu )
~~~~
sudo -i
./post-install.sh -b -r "4.1.12"
~~~~
- Remove LinuxBaseBox Scripts
~~~~
rm -r linuxbasebox
~~~~

For more parameters of the scripts, have a look in the help.