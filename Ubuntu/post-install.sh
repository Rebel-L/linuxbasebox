#!/bin/bash

###################################
# BaseBox Script for Ubuntu 14.10 #
###################################
# Config
VERSION=1.0.1
DISTRIBUTION="Ubuntu 14 (Tested with 14.04LTS & 14.10)"
VERBOSE=0
INSTALLCHEF=1
INSTALLPUPPET=1
INSTALLSSH=1
USEVAGRANT=0
PATH_TO_TMP=/tmp
PATH_TO_SCRIPT=`pwd`
REGEX_VERSION="[0-9]{1,2}[\.][0-9]{1,2}[\.][0-9]{1,2}"
CHEF_VERSION=0
PUPPET_VERSION=0
VIRTUALBOX_VERSION=0
CONFIG_SUDOERS="/etc/sudoers"

#############
# Functions #
#############
# print the header
printHeader(){
	echo
	echo "Base Box Script for $DISTRIBUTION"
}

# prints the footer
printFooter(){
	echo; echo
}

# prints an error message
printFailure(){
	echo -en '\E[40;33m'"\033[1m$1\033[0m" >&2  # Yellow
	tput sgr0
}

# prints an error message
printError(){
	echo -en '\E[40;31m'"\033[1m$1\033[0m" >&2  # Red
	tput sgr0
}

# prints an success message
printSuccess(){
	echo -en '\E[40;32m'"\033[1m$1\033[0m"   # Green
	tput sgr0
}

# prints the help
printHelp (){
	printHeader
	cat help.txt
	printFooter
	exit 0
}

# prints the version
printVersion(){
	printHeader
	echo "Version: $VERSION"
	printFooter
	exit 0
}

# print start of script
printStart(){
	printHeader
	printVerboseOnly "Script starting ..."
	echo
	echo
}

# print end of script
printEnd(){
	echo
	echo "Installed Chef Version: $CHEF_VERSION"
	echo "Installed PuppetVersion: $PUPPET_VERSION"
	printSuccess "Setting up base box for $DISTRIBUTION was successful"
	printFooter
	exit 0
}

# print verbose text
printVerboseOnly(){
	if [ $VERBOSE -eq 1 ]
	then
		echo $1
	fi
}

# prints an error message an exits the script
handleError(){
	printError "$1 Execution canceled!"
	printFooter
	if [ -n $2 ]
	then
		exit $2
	else
		exit 1
	fi
}

# checks that script is executed with sudo
checkSudo(){
	if [ $UID -ne 0 ]
	then
		handleError "Script must be executed as SUDO."
	fi
}

# installs ssh depending on arguments
installSsh(){
	if [ $INSTALLSSH -eq 1 ]
	then
		printVerboseOnly "Install ssh client ..."
		apt-get -y install ssh
		echo
	fi
}

# Install VirtualBox Guest Additions
installVirtualBoxGuestAdditions(){
	if [ $USEVAGRANT -eq 1 ] && [ $VIRTUALBOX_VERSION != "0" ]; then
		printVerboseOnly "Installs guest additions for virtual box version $VIRTUALBOX_VERSION ..."
		GUESTADDITIONS_IMAGE=VBoxGuestAdditions_$VIRTUALBOX_VERSION.iso
		cd $PATH_TO_TMP
		wget http://download.virtualbox.org/virtualbox/$VIRTUALBOX_VERSION/$GUESTADDITIONS_IMAGE

		if [ ! -f $GUESTADDITIONS_IMAGE ]; then
			handleError "Error on downloading $GUESTADDITIONS_IMAGE. Please check the version you entered does exist."
		fi

		mount -o loop $GUESTADDITIONS_IMAGE /mnt
		sh /mnt/VBoxLinuxAdditions.run
		umount /mnt
		rm $GUESTADDITIONS_IMAGE
		cd $PATH_TO_SCRIPT
		echo
	fi
}

# installs the chef client
installChefClient(){
	if [ $INSTALLCHEF -eq 1 ]
	then
		printVerboseOnly "Installs the chef client ..."
		cd $PATH_TO_TMP
		curl -L https://www.opscode.com/chef/install.sh | bash
		cd $PATH_TO_SCRIPT

		CHEF_VERSION=`chef-solo -v 2>&1 | egrep -o "$REGEX_VERSION"`
		if [ -n $CHEF_VERSION ]
		then
			printVerboseOnly "Installed Chef Version: $CHEF_VERSION"
		else
			printFailure "Chef was not installed successfully. Install it manually"
		fi
		echo
	fi
}

# install puppet client()
installPuppetClient(){
	if [ $INSTALLPUPPET -eq 1 ]; then
		printVerboseOnly "Installs the puppet client ..."
		cd $PATH_TO_TMP
		PUPPET_DEB="puppetlabs-release-trusty.deb"
		wget https://apt.puppetlabs.com/$PUPPET_DEB
		dpkg -i puppetlabs-release-trusty.deb
		apt-get -y update
		apt-get -y install puppet
		rm $PUPPET_DEB
		cd $PATH_TO_SCRIPT

		PUPPET_VERSION=`puppet -V 2>&1 | egrep -o "$REGEX_VERSION"`
		if [ -n $PUPPET_VERSION ]; then
			printVerboseOnly "Installed Puppet Version: $CHEF_VERSION"
		else
			printFailure "Puppet was not installed successfully. Install it manually"
		fi
		echo
	fi
}

# installs vagrant keys
installVagrantKeys(){
	if [ $USEVAGRANT -eq 1 ]; then
		printVerboseOnly "Installs keys for vagrant ..."
		mkdir -m700 /home/vagrant/.ssh
		cat ../etc/ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
		chmod 600 /home/vagrant/.ssh/authorized_keys
		chown -R vagrant:vagrant /home/vagrant/.ssh
		echo
	fi
}

# cleaning up disk
cleanUp(){
	printVerboseOnly "Cleans up machine ..."
	apt-get clean
	apt-get autoclean
	apt-get -y autoremove

	if [ $USEVAGRANT -eq 1 ]; then
		echo "Zero out the free space to save space in the final image:"
		dd if=/dev/zero of=/EMPTY bs=1M
		rm -f /EMPTY
	fi

	NETWORK_FILE="/etc/udev/rules.d/70-persistent-net.rules"
	if [ -f $NETWORK_FILE ]; then
		rm $NETWORK_FILE
	fi
	echo
}


# Setup sudo to allow no-password sudo for "admin"
setupSudoers(){
	if [ $USEVAGRANT -eq 1 ]; then
		printVerboseOnly "Setup Sudoers ..."
		cp $CONFIG_SUDOERS $CONFIG_SUDOERS.orig
		echo '' >> $CONFIG_SUDOERS
		echo 'vagrant ALL=(ALL) NOPASSWD:ALL' >> $CONFIG_SUDOERS
		echo '' >> $CONFIG_SUDOERS
		echo 'Defaults:vagrant !requiretty' >> $CONFIG_SUDOERS
	fi
}


#####################
# Extract Arguments #
#####################
ARGS=$(getopt -o bcdhpr:sv -l "vagrant-base-box,chef-only,verbose,help,puppet-only,virtualbox-version:,no-ssh,version" -n "post-install.sh" -- "$@");

#Bad arguments
if [ $? -ne 0 ];
then
	exit 1
fi

eval set -- "$ARGS";

# Parse arguments
while true; do
	case "$1" in
		-b|--vagrant-base-box)
			USEVAGRANT=1
			shift;
			;;
		-c|--chef-only)
			INSTALLPUPPET=0
			shift;
			;;
		-d|--verbose)
			VERBOSE=1
			shift;
			;;
		-h|--help)
			shift;
			printHelp
			;;
		-p|--puppet-only)
			INSTALLCHEF=0
			shift;
			;;
		-r|--virtualbox-version)
			shift;
			if [ -n $1 ]
			then
				VIRTUALBOX_VERSION=$1
				shift;
			fi
			;;
		-s|--no-ssh)
			INSTALLSSH=0
			shift;
			;;
		-v|--version)
			printVersion
			shift;
			;;
		--)
			shift;
			break;
			;;
	esac
done

####################
# Script Execution #
####################
printStart
checkSudo

# Save the build time
date > /etc/basebox_build_time

printVerboseOnly "Updating / Upgrading system files ..."
apt-get -y update
apt-get -y upgrade

printVerboseOnly "Install htop .."
apt-get -y install htop

printVerboseOnly "Install linux headers"
apt-get -y install linux-headers-$(uname -r) build-essential
echo

installSsh
setupSudoers
installVirtualBoxGuestAdditions
installChefClient
echo
installPuppetClient
installVagrantKeys

cleanUp

printEnd